package com.javafxcontrols.localizedNumberField;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.stage.Stage;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.FlowPane;

public class Main extends Application {
	@Override
	public void start(Stage stage) {
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(getClass().getResource("LocalizedNumberField.fxml"));
			Parent root = loader.load();
			//FlowPane root = new FlowPane();
			
//			LocalizedNumberField lnf = new LocalizedNumberField(-10.0, 10.0, 0.0, 3);
//			lnf.setNumber(6.777777777777777);
//			root.getChildren().add(lnf);
			
			Scene scene = new Scene(root);
			stage.setTitle("Localized Number Field demo");
			stage.setScene(scene);
			stage.setMaximized(true);
			stage.show();
			
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		launch(args);
	}
}
