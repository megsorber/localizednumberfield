package com.javafxcontrols.localizedNumberField;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Locale;
import java.util.function.UnaryOperator;

import javafx.beans.NamedArg;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import javafx.scene.control.TextFormatter;
import javafx.scene.control.TextFormatter.Change;
import javafx.util.converter.DoubleStringConverter;

public class LocalizedNumberField extends TextField {
	
	private SimpleDoubleProperty number;
	@FXML public Double min = -Double.MAX_VALUE;
	private Double max = Double.MAX_VALUE;
	private int maxDecimals = 5;
	private Locale locale = Locale.ENGLISH;
	private TextFormatter<Double> textFormatter;
	private DecimalFormat localeFormatter;
		
	public LocalizedNumberField() {
		super();
		number = new SimpleDoubleProperty(0);
		initialize();
	}
	
	public LocalizedNumberField(@NamedArg("number") Double number) {
		super();
		this.number = new SimpleDoubleProperty(number);
		initialize();
	}
	
	public LocalizedNumberField(@NamedArg("locale") String locale) {
		super();
		number = new SimpleDoubleProperty(0);
		this.locale = new Locale(locale);
		initialize();
	}
	
	public LocalizedNumberField(@NamedArg("min") Double min, @NamedArg("max") Double max) {
		super();
		this.min = min;
		this.max = max;
		number = new SimpleDoubleProperty(min + (max-min)/2);
		initialize();
	}
	
	public LocalizedNumberField(@NamedArg("min") Double min, @NamedArg("max") Double max, 
			@NamedArg("number") Double number) {
		super();
		this.min = min;
		this.max = max;
		this.number = new SimpleDoubleProperty(number);
		initialize();
	}
	
	public LocalizedNumberField(@NamedArg("min") Double min, @NamedArg("max") Double max, 
			@NamedArg("number") Double number, @NamedArg("maxDecimals") int maxDecimals) {
		super();
		this.min = min;
		this.max = max;
		this.number = new SimpleDoubleProperty(number);
		this.maxDecimals = maxDecimals;
		initialize();
	}
	
	public LocalizedNumberField(@NamedArg("min") Double min, @NamedArg("max") Double max, 
			@NamedArg("number") Double number, @NamedArg("locale") String locale) {
		this.min = min;
		this.max = max;
		this.number = new SimpleDoubleProperty(number);
		this.locale = new Locale(locale);
		initialize();
	}
	
	public LocalizedNumberField(@NamedArg("min") Double min, @NamedArg("max") Double max, 
			@NamedArg("number") Double number, @NamedArg("maxDecimals") int maxDecimals, 
			@NamedArg("locale") String locale) {
		this.min = min;
		this.max = max;
		this.number = new SimpleDoubleProperty(number);
		this.maxDecimals = maxDecimals;
		this.locale = new Locale(locale);
		initialize();
	}
	
	private void initialize() {
		//todo throw error if max !> min
		//todo throw out of bounds error if number is not between min and max
		initFormatterPattern();
		setPrefColumnCount(calculatePrefColumnCount());
		this.setMaxWidth(this.getPrefWidth());

		textProperty().addListener(this::numberTextchanged);
		number.addListener(this::numberChanged);
	}
	
	private void initFormatterPattern() {
		
		//no need to worry about the negative sign:
		//The negative subpattern is optional; if absent, then the positive subpattern 
		//prefixed with the localized minus sign ('-' in most locales) is used as the 
		//negative subpattern. That is, "0.00" alone is equivalent to "0.00;-0.00"
		NumberFormat nf = NumberFormat.getNumberInstance(locale);
		localeFormatter = (DecimalFormat)nf;
		
		textFormatter = new TextFormatter<Double>(new localizedDoubleStringConverter(), number.doubleValue(), new DoubleFilter());
		this.setTextFormatter(textFormatter);
		
		String formatterPattern = "";
				
		//before the decimal
		double tempMax = max;
		int numberOfDigits=1;
		while(tempMax > 1) {
			numberOfDigits++;
			tempMax = tempMax/10;
		}
		
		int digitsBeforFirstSeperator = numberOfDigits%localeFormatter.getGroupingSize();
		for(int i=0; i<digitsBeforFirstSeperator; i++) {
			formatterPattern += "#";
		}
		int numberOfDigitGroups = (numberOfDigits-digitsBeforFirstSeperator)/localeFormatter.getGroupingSize();
		String group = "";
		for(int i=0; i<localeFormatter.getGroupingSize(); i++) {
			group += "#";
		}
		for(int i=0; i<numberOfDigitGroups; i++) {
			formatterPattern += "," + group;
		}
		
		//the last # should be 0 instead so it shows with a decimal
		if(numberOfDigits>0)
			formatterPattern = formatterPattern.substring(0, formatterPattern.length()-1) +"0";
		else
			formatterPattern = "0";
		
		//decimal place
		if(maxDecimals > 0) {
			formatterPattern += ".";
		}
		
		//decimals
		for(int i=0; i<maxDecimals; i++) {
			formatterPattern += "#";
		}
		
		System.out.println(formatterPattern);
		localeFormatter.applyPattern(formatterPattern);
	}
	
	private void numberTextchanged(ObservableValue<? extends String> observable, String oldValue, String newText) {
		if(validNonNumberText(newText)) {
			number.set(Double.NaN);
			return;
		}
		try {
			Double newNumber = localeFormatter.parse(newText).doubleValue();
			number.set(newNumber);
		} catch (ParseException e) {
			return;
		}
	}
	
	private void numberChanged(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
		//check if the number changed due to the text changing, if so don't touch anything
		//since javaFX will sometimes throw an error
		try {
			if(localeFormatter.parse(this.getText()).doubleValue() != newValue.doubleValue()) {
				this.setText(localeFormatter.format(newValue));
			}
		} catch (ParseException e) {
			if(!((Double) oldValue).isNaN())
				number.set((double) oldValue);
		}
	}

	public class DoubleFilter implements UnaryOperator<TextFormatter.Change> {
	    @Override
	    public Change apply(TextFormatter.Change change) {
    		String newText = change.getControlNewText();
	    	if(validate(newText))
	    		return change;
	    	else
	    		return null;
	    }
	}
	
	class localizedDoubleStringConverter extends DoubleStringConverter{
		@Override
		public Double fromString(String newText) {
			try{
				if(validate(newText)) {
					return localeFormatter.parse(newText).doubleValue();
				}else{
					//The NumberFormatException will result in the
					//previous value being used.
					throw new NumberFormatException();
				}
			} catch (ParseException e) {
				throw new NumberFormatException();
			}
		}
		
		@Override
		public String toString(Double number) {
			return localeFormatter.format(number.doubleValue());
		}
	}
	
	private boolean validate(String newText) {
		if(validNonNumberText(newText)) {
			return true;
    	}

		if(validText(newText) 
			&& validNumber(newText) 
			&& validDecimals(newText)) {
			return true;
		}else {
			return false;
		}

/*		System.out.println(newText);
		System.out.println("validText: " + validText(newText));
		System.out.println("validNumber: " + validNumber(newText));
		System.out.println("validDecimals: " + validDecimals(newText));
		System.out.println();*/
	}

	private boolean validNonNumberText(String newText) {
		String negative = localeFormatter.getNegativePrefix();
		Character decimal = localeFormatter.getDecimalFormatSymbols().getDecimalSeparator();
		return newText.equals("") 
				|| newText.equals(negative)
				|| newText.equals(decimal.toString())
				|| newText.equals(negative+decimal);
	}
	
	private boolean validText(String newText) {
		//if the number can be negative allow a negative prefix as the first character
		if(min<0) {
			String negative = localeFormatter.getNegativePrefix();
			if(newText.substring(0, 1).equals(negative))
				newText = newText.substring(1);
		}
		
		newText = newText.replaceAll("\\d", "");
		
		Character decimalSeparator = localeFormatter.getDecimalFormatSymbols().getDecimalSeparator();

		int decimalIndex = newText.indexOf(decimalSeparator);
		String integerPart;
		String decimalPart = null;
		if(decimalIndex>-1) {
			integerPart = newText.substring(0, decimalIndex);
			decimalPart = newText.substring(decimalIndex+1);
		}
		else {
			integerPart = newText;
		}
		
		//allow separators 
		Character groupingSeparator = localeFormatter.getDecimalFormatSymbols().getGroupingSeparator();
		integerPart = integerPart.replaceAll(groupingSeparator.toString(), "");
		boolean isValid = integerPart.length()==0;
		
		//there should be no separators in the decimal part, only numbers
		if(decimalPart != null) {
			isValid = isValid && decimalPart.length()==0;
		}
		
		//TODO
		return isValid;
	}
	
	private boolean validNumber(String textNumber) {
		try {
			Double number = localeFormatter.parse(textNumber).doubleValue();
			return number>=min && number<=max;
		} catch (ParseException e) {
			return false;
		}
	}
	
	private boolean validDecimals(String number) {
		char decimalSeperator = localeFormatter.getDecimalFormatSymbols().getDecimalSeparator();
		int decimalIndex = number.indexOf(decimalSeperator);
		if(decimalIndex == -1) //no decimals
			return true;
		int numDecimals = number.substring(decimalIndex).length()-1; //exclude the decimal separator
		return numDecimals<=maxDecimals;
	}
	
	private int calculatePrefColumnCount() {
		int prefColumnCount = 0;
		
		//account for "-" sign
		if(min < 0)
			prefColumnCount++;
				
		//before the decimal
		double tempMax = max;
		while(tempMax > 1) {
			prefColumnCount++;
			tempMax = tempMax/10;
		}
		
		//decimal place
		if(maxDecimals > 0) {
			prefColumnCount++;
		}
		//decimals
		prefColumnCount += maxDecimals;
		for(int i=0; i<maxDecimals; i++) {
		}
		
		return prefColumnCount;
	}
	
	public void setMin(Double min) {
		this.min = min;
		setPrefColumnCount(calculatePrefColumnCount());
	}
	public double getMin() {
		return min;
	}
	
	public void setMax(Double max) {
		this.max = max;
		setPrefColumnCount(calculatePrefColumnCount());
	}
	public double getMax() {
		return max;
	}
	
	public void setMaxDecimals(int maxDecimals) {
		this.maxDecimals = maxDecimals;
		setPrefColumnCount(calculatePrefColumnCount());
	}
	public int getMaxDecimals() {
		return maxDecimals;
	}
	
	public void setLocale(Locale locale) {
		this.locale = locale;
		initFormatterPattern();
	}
	public Locale getLocale() {
		return locale;
	}
	
	@Override
	public String toString() {
		return "LocalizedNumberField: min="+min
				+", max="+max
				+", number="+number.get()
				+", maxDecimals="+maxDecimals
				+", local="+locale;
	}

	public final SimpleDoubleProperty numberProperty() {
		return this.number;
	}
	public final double getNumber() {
		return this.numberProperty().get();
	}
	
	//TODO: throw out of bounds error
	public final void setNumber(double number) {
		double decimalPlaceMultiplyer = Math.pow(10, maxDecimals);
		number=Math.round(number*decimalPlaceMultiplyer)/decimalPlaceMultiplyer;
		this.numberProperty().set(number);
	}
}
