package com.javafxcontrols.localizedNumberField;

import javafx.fxml.FXML;
import javafx.scene.control.Label;

public class Controller {
	@FXML protected LocalizedNumberField exampleLNF;
	@FXML protected Label exampleLabel;
	
	@FXML
	public void initialize() {
		exampleLabel.textProperty().bind(exampleLNF.numberProperty().asString());
	}
}
