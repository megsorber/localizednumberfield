module com.javafxcontrols.localizedNumberField {
    requires transitive javafx.base;
    requires transitive javafx.fxml;
    requires transitive javafx.controls;
    requires transitive javafx.graphics;
    
    exports com.javafxcontrols.localizedNumberField;
    opens com.javafxcontrols.localizedNumberField;
}